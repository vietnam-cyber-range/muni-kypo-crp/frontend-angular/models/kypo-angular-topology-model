/**
 * Node of special type
 */
import { Connectable } from './connectable';
import { Node } from './node';

export class SpecialNode extends Node {
  constructor() {
    super();
  }
}
