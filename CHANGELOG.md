### 16.0.0 Update to Angular 16.
* 8fa44ca -- [CI/CD] Update packages.json version based on GitLab tag.
*   cb58d7e -- Merge branch '28-update-to-angular-16' into 'master'
|\  
| * 17fa643 -- Update to Angular 16
|/  
* 4de33ea -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* ac08fe6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7425c97 -- Merge branch '27-update-to-angular-15' into 'master'
|\  
| * a7207cb -- Update to Angular 15
|/  
* ea20343 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d3da90e -- [CI/CD] Update packages.json version based on GitLab tag.
*   3f57873 -- Merge branch '26-create-new-node-type-for-docker-containers' into 'master'
|\  
| * 342ddfb -- Update container size check
* | dc79c25 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 475a7c4 -- [CI/CD] Update packages.json version based on GitLab tag.
* | afeda26 -- Merge branch 'container-fix' into 'master'
|\| 
| * 2cc83cc -- Fix containers tooltip visibility
* | efd1ebb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | a3d8485 -- [CI/CD] Update packages.json version based on GitLab tag.
* | 1ee2533 -- Merge branch '26-create-new-node-type-for-docker-containers' into 'master'
|\| 
| * d20a274 -- Fixed formatting of docker container text.
| * 985ec5a -- Updated version.
| * 9bf8de1 -- Removed container-host node and moved its information to host node. Modified host node to string function.
| * 20e0d3a -- Updated version.
| * 2650000 -- Add and export a new host-container-node object.
| * b5767b2 -- Added docker type to physical role of node.
|/  
* 0485a7f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b4dceb9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba1fe0c -- Merge branch '25-fix-module-name' into 'master'
|\  
| * 7edadd6 -- Resolve "Fix module name"
|/  
* eb315a7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2a795fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c3f2ee -- Merge branch '24-rename-from-kypo2-to-kypo' into 'master'
|\  
| * bc1ea3f -- Resolve "Rename from kypo2 to kypo"
|/  
* e85cd35 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fa50859 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c65341 -- Merge branch '23-update-to-angular-14' into 'master'
|\  
| * e886a96 -- Resolve "Update to Angular 14"
|/  
* b5e3c42 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c94d0d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d8dcf41 -- Merge branch '22-update-to-angular-13' into 'master'
|\  
| * 012ddc5 -- Resolve "Update to Angular 13"
|/  
* 92f7b0d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 15.0.0 Update to Angular 15.
* ac08fe6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7425c97 -- Merge branch '27-update-to-angular-15' into 'master'
|\  
| * a7207cb -- Update to Angular 15
|/  
* ea20343 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* d3da90e -- [CI/CD] Update packages.json version based on GitLab tag.
*   3f57873 -- Merge branch '26-create-new-node-type-for-docker-containers' into 'master'
|\  
| * 342ddfb -- Update container size check
* | dc79c25 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 475a7c4 -- [CI/CD] Update packages.json version based on GitLab tag.
* | afeda26 -- Merge branch 'container-fix' into 'master'
|\| 
| * 2cc83cc -- Fix containers tooltip visibility
* | efd1ebb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | a3d8485 -- [CI/CD] Update packages.json version based on GitLab tag.
* | 1ee2533 -- Merge branch '26-create-new-node-type-for-docker-containers' into 'master'
|\| 
| * d20a274 -- Fixed formatting of docker container text.
| * 985ec5a -- Updated version.
| * 9bf8de1 -- Removed container-host node and moved its information to host node. Modified host node to string function.
| * 20e0d3a -- Updated version.
| * 2650000 -- Add and export a new host-container-node object.
| * b5767b2 -- Added docker type to physical role of node.
|/  
* 0485a7f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b4dceb9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba1fe0c -- Merge branch '25-fix-module-name' into 'master'
|\  
| * 7edadd6 -- Resolve "Fix module name"
|/  
* eb315a7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2a795fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c3f2ee -- Merge branch '24-rename-from-kypo2-to-kypo' into 'master'
|\  
| * bc1ea3f -- Resolve "Rename from kypo2 to kypo"
|/  
* e85cd35 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fa50859 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c65341 -- Merge branch '23-update-to-angular-14' into 'master'
|\  
| * e886a96 -- Resolve "Update to Angular 14"
|/  
* b5e3c42 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c94d0d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d8dcf41 -- Merge branch '22-update-to-angular-13' into 'master'
|\  
| * 012ddc5 -- Resolve "Update to Angular 13"
|/  
* 92f7b0d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 14.0.5 Update containers size check
* d3da90e -- [CI/CD] Update packages.json version based on GitLab tag.
*   3f57873 -- Merge branch '26-create-new-node-type-for-docker-containers' into 'master'
|\  
| * 342ddfb -- Update container size check
* | dc79c25 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | 475a7c4 -- [CI/CD] Update packages.json version based on GitLab tag.
* | afeda26 -- Merge branch 'container-fix' into 'master'
|\| 
| * 2cc83cc -- Fix containers tooltip visibility
* | efd1ebb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | a3d8485 -- [CI/CD] Update packages.json version based on GitLab tag.
* | 1ee2533 -- Merge branch '26-create-new-node-type-for-docker-containers' into 'master'
|\| 
| * d20a274 -- Fixed formatting of docker container text.
| * 985ec5a -- Updated version.
| * 9bf8de1 -- Removed container-host node and moved its information to host node. Modified host node to string function.
| * 20e0d3a -- Updated version.
| * 2650000 -- Add and export a new host-container-node object.
| * b5767b2 -- Added docker type to physical role of node.
|/  
* 0485a7f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b4dceb9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba1fe0c -- Merge branch '25-fix-module-name' into 'master'
|\  
| * 7edadd6 -- Resolve "Fix module name"
|/  
* eb315a7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2a795fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c3f2ee -- Merge branch '24-rename-from-kypo2-to-kypo' into 'master'
|\  
| * bc1ea3f -- Resolve "Rename from kypo2 to kypo"
|/  
* e85cd35 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fa50859 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c65341 -- Merge branch '23-update-to-angular-14' into 'master'
|\  
| * e886a96 -- Resolve "Update to Angular 14"
|/  
* b5e3c42 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c94d0d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d8dcf41 -- Merge branch '22-update-to-angular-13' into 'master'
|\  
| * 012ddc5 -- Resolve "Update to Angular 13"
|/  
* 92f7b0d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 14.0.4 Fix containers visibility check.
* 475a7c4 -- [CI/CD] Update packages.json version based on GitLab tag.
*   afeda26 -- Merge branch 'container-fix' into 'master'
|\  
| * 2cc83cc -- Fix containers tooltip visibility
* | efd1ebb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* | a3d8485 -- [CI/CD] Update packages.json version based on GitLab tag.
* | 1ee2533 -- Merge branch '26-create-new-node-type-for-docker-containers' into 'master'
|\| 
| * d20a274 -- Fixed formatting of docker container text.
| * 985ec5a -- Updated version.
| * 9bf8de1 -- Removed container-host node and moved its information to host node. Modified host node to string function.
| * 20e0d3a -- Updated version.
| * 2650000 -- Add and export a new host-container-node object.
| * b5767b2 -- Added docker type to physical role of node.
|/  
* 0485a7f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b4dceb9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba1fe0c -- Merge branch '25-fix-module-name' into 'master'
|\  
| * 7edadd6 -- Resolve "Fix module name"
|/  
* eb315a7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2a795fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c3f2ee -- Merge branch '24-rename-from-kypo2-to-kypo' into 'master'
|\  
| * bc1ea3f -- Resolve "Rename from kypo2 to kypo"
|/  
* e85cd35 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fa50859 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c65341 -- Merge branch '23-update-to-angular-14' into 'master'
|\  
| * e886a96 -- Resolve "Update to Angular 14"
|/  
* b5e3c42 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c94d0d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d8dcf41 -- Merge branch '22-update-to-angular-13' into 'master'
|\  
| * 012ddc5 -- Resolve "Update to Angular 13"
|/  
* 92f7b0d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 14.0.3 Add container field to host node.
* a3d8485 -- [CI/CD] Update packages.json version based on GitLab tag.
*   1ee2533 -- Merge branch '26-create-new-node-type-for-docker-containers' into 'master'
|\  
| * d20a274 -- Fixed formatting of docker container text.
| * 985ec5a -- Updated version.
| * 9bf8de1 -- Removed container-host node and moved its information to host node. Modified host node to string function.
| * 20e0d3a -- Updated version.
| * 2650000 -- Add and export a new host-container-node object.
| * b5767b2 -- Added docker type to physical role of node.
|/  
* 0485a7f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* b4dceb9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba1fe0c -- Merge branch '25-fix-module-name' into 'master'
|\  
| * 7edadd6 -- Resolve "Fix module name"
|/  
* eb315a7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2a795fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c3f2ee -- Merge branch '24-rename-from-kypo2-to-kypo' into 'master'
|\  
| * bc1ea3f -- Resolve "Rename from kypo2 to kypo"
|/  
* e85cd35 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fa50859 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c65341 -- Merge branch '23-update-to-angular-14' into 'master'
|\  
| * e886a96 -- Resolve "Update to Angular 14"
|/  
* b5e3c42 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c94d0d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d8dcf41 -- Merge branch '22-update-to-angular-13' into 'master'
|\  
| * 012ddc5 -- Resolve "Update to Angular 13"
|/  
* 92f7b0d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 14.0.2 Fix module name.
* b4dceb9 -- [CI/CD] Update packages.json version based on GitLab tag.
*   ba1fe0c -- Merge branch '25-fix-module-name' into 'master'
|\  
| * 7edadd6 -- Resolve "Fix module name"
|/  
* eb315a7 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 2a795fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c3f2ee -- Merge branch '24-rename-from-kypo2-to-kypo' into 'master'
|\  
| * bc1ea3f -- Resolve "Rename from kypo2 to kypo"
|/  
* e85cd35 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fa50859 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c65341 -- Merge branch '23-update-to-angular-14' into 'master'
|\  
| * e886a96 -- Resolve "Update to Angular 14"
|/  
* b5e3c42 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c94d0d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d8dcf41 -- Merge branch '22-update-to-angular-13' into 'master'
|\  
| * 012ddc5 -- Resolve "Update to Angular 13"
|/  
* 92f7b0d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 14.0.1 Rename from kypo2 to kypo.
* 2a795fd -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c3f2ee -- Merge branch '24-rename-from-kypo2-to-kypo' into 'master'
|\  
| * bc1ea3f -- Resolve "Rename from kypo2 to kypo"
|/  
* e85cd35 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* fa50859 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c65341 -- Merge branch '23-update-to-angular-14' into 'master'
|\  
| * e886a96 -- Resolve "Update to Angular 14"
|/  
* b5e3c42 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c94d0d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d8dcf41 -- Merge branch '22-update-to-angular-13' into 'master'
|\  
| * 012ddc5 -- Resolve "Update to Angular 13"
|/  
* 92f7b0d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 14.0.0 Update to Angular 14.
* fa50859 -- [CI/CD] Update packages.json version based on GitLab tag.
*   7c65341 -- Merge branch '23-update-to-angular-14' into 'master'
|\  
| * e886a96 -- Resolve "Update to Angular 14"
|/  
* b5e3c42 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c94d0d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d8dcf41 -- Merge branch '22-update-to-angular-13' into 'master'
|\  
| * 012ddc5 -- Resolve "Update to Angular 13"
|/  
* 92f7b0d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 13.0.0 Update to Angular 13, CI/CD update
* c94d0d1 -- [CI/CD] Update packages.json version based on GitLab tag.
*   d8dcf41 -- Merge branch '22-update-to-angular-13' into 'master'
|\  
| * 012ddc5 -- Resolve "Update to Angular 13"
|/  
* 92f7b0d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 12.0.3 Add new node of "special" type (for internet nodes)
* 5582774 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4cf3a0f -- Merge branch '21-trigger-new-tag-creation' into 'master'
|\  
| * e8e8689 -- Changed version file
|/  
*   a6a66df -- Merge branch '20-revert-package-version-for-tagging' into 'master'
|\  
| * 362cccd -- Lib version changed
|/  
*   fd3fef1 -- Merge branch '19-add-attributes-ostype-and-guiaccess-to-the-hostnode-and-routernode' into 'master'
|\  
| * b291268 -- Added attributes osType and guiAccess to the HostNode and RouterNode
* |   d9d4abb -- Merge branch '18-add-new-node-of-special-type-internet' into 'master'
|\ \  
| |/  
|/|   
| * 4c72a03 -- Resolve "Add new node of special type (internet)"
|/  
*   9c6e85e -- Merge branch '17-add-license-file' into 'master'
|\  
| * 9487ca2 -- Add license file
|/  
* 9e1e55a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
### 12.0.2 Update gitlab CI
* 58a7e81 -- [CI/CD] Update packages.json version based on GitLab tag.
*   36748ad -- Merge branch '16-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 8fcde06 -- Update gitlab CI config
|/  
* 4f332b1 -- Update project package.json version based on GitLab tag. Done by CI
*   1ab268a -- Merge branch '15-update-to-angular-12' into 'master'
|\  
| * 6bda6ff -- Resolve "Update to Angular 12"
|/  
* 81eb03d -- Update project package.json version based on GitLab tag. Done by CI
*   9b570d2 -- Merge branch '14-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 3162122 -- Updated peerDependency
|/  
* e6d9874 -- Update project package.json version based on GitLab tag. Done by CI
*   f27c59e -- Merge branch '13-update-to-angular-11' into 'master'
|\  
| * ee07b6a -- Update to Angular 11
|/  
*   ea5516e -- Merge branch '12-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 2ed49cc -- recreate package-lock
|/  
*   497b15f -- Merge branch '10-migrate-from-tslint-to-eslint' into 'master'
|\  
| * d447451 -- Resolve "Migrate from tslint to eslint"
|/  
* 42150e4 -- Update project package.json version based on GitLab tag. Done by CI
*   f9814f5 -- Merge branch '11-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 537a170 -- Rename the package scope
|/  
* a6c5b3e -- Update project package.json version based on GitLab tag. Done by CI
* dc3f9e5 -- Fix CI script
* a89e5a2 -- Fix typo CI script
* d0889bd -- fix git merge conflict in failed CI pipeline
* 22e3f59 -- fix deploy ci script
*   675ee69 -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\  
| * 543bc13 -- Fix typo in package name
* | 9f201cc -- Merge branch '9-rename-package-to-kypo-topology-model' into 'master'
|\| 
| * 93edb74 -- Rename the package
|/  
*   ba26c1b -- Merge branch '8-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 5cdd303 -- Remove personal info from README
|/  
*   5164063 -- Merge branch '7-use-cypress-image-in-ci' into 'master'
|\  
| * 93b92ad -- Resolve "Use cypress image in CI"
|/  
* 3a895ad -- Update project package.json version based on GitLab tag. Done by CI
*   80475fc -- Merge branch '6-update-to-angular-10' into 'master'
|\  
| * 661f3ec -- Resolve "Update to Angular 10"
|/  
* 7c79f02 -- Merge branch '5-make-the-ci-build-stage-build-with-prod-param' into 'master'
* f737587 -- Update .gitlab-ci.yml
