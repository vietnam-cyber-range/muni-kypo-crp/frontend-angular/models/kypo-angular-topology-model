16.0.0 Update to Angular 16.
15.0.0 Update to Angular 15.
14.0.5 Update containers size check
14.0.4 Fix containers visibility check.
14.0.3 Add container field to host node.
14.0.2 Fix module name.
14.0.1 Rename from kypo2 to kypo.
14.0.0 Update to Angular 14.
13.0.0 Update to Angular 13, CI/CD update
12.0.3 Add new node of "special" type (for internet nodes)
12.0.2 Update gitlab CI
